#include <iostream>
#include <cmath>

/* avg compilation time from /usr/bin/time
g++ -o halfSearchPerformance.out halfScrearchPerformance.cpp
0.23user 0.03system 0:00.27elapsed 98%CPU (0avgtext+0avgdata 54572maxresident)k
0inputs+32outputs (0major+17411minor)pagefaults 0swaps
*/

/*
avg run time from /usr/bin/time
HalfSearch
value found at pos: 6782
0.00user 0.00system 0:00.01elapsed 90%CPU (0avgtext+0avgdata 7268maxresident)k
0inputs+0outputs (0major+1102minor)pagefaults 0swaps
*/


int HalfSearch (int valueToFind,int data[], int size, int startPos, int EndPos);

int main() {

    int sortedArray[1000000];
    int size = (sizeof(sortedArray)/sizeof(*sortedArray));
    
    //0 -> 999999
    for(int i = 0; i < size; i++)
    {
        sortedArray[i] = i;
    }


        std::cout << "HalfSearch" << '\n';
        int startAt = 0;
        int EndPos = size -1;
        int middle = HalfSearch (6782,sortedArray,size, startAt,EndPos);

    if(middle >=0) {
    std::cout << "value found at pos: " << middle << '\n';
    } 
    else {
    std::cout << "value not found" << '\n';
    }

        
}

int HalfSearch (int valueToFind,int data[], int size, int startPos, int EndPos) {    
    while(startPos <= EndPos){
    int middle = std::floor((startPos + EndPos) / 2);

        if(data[middle] < valueToFind)  {
            startPos = middle + 1;
        }
        else if (data[middle] > valueToFind) {
            EndPos = middle-1;
        }
        else {
            return middle;
        }
    }
    //value not found
    return -1;


}
