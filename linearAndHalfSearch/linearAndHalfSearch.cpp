#include <iostream>
#include <cmath>
/* linear and half-interval search*/

/*
g++ -o output filename.cpp 
*/
int LinearSearch(int data[], int size, int searchValue);
int HalfSearch (int valueToFind,int data[], int size, int startPos, int EndPos);

int main() {


    int array[] = {8,2,3,4,5};
    int sortedArray[] = {1,4,6,11,13,16,19,20,25,27,29,30,32,36,39,42,45,48,49,53};
    
    int indexOfValue = LinearSearch(array,5, 4);

    std::cout << "Linear Search" << '\n';
    if(indexOfValue >=0) {
    std::cout << "index of found value is" << indexOfValue << '\n';
    } 
    else {
    std::cout << "value not found" << '\n';
    }

        std::cout << "HalfSearch" << '\n';
        int size = (sizeof(sortedArray)/sizeof(*sortedArray));
        int startAt = 0;
        int EndPos = size -1;
        int middle = HalfSearch (12,sortedArray,size, startAt,EndPos);

    if(middle >=0) {
    std::cout << "value found at pos: " << middle << '\n';
    } 
    else {
    std::cout << "value not found" << '\n';
    }

        
}

int LinearSearch(int data[], int size, int searchValue) {
  
    for(int i = 0; i < size; i++)
    {
        if(searchValue == data[i]) {
            return i;
        }
    }

    //value not found
    return -1;
}

int HalfSearch (int valueToFind,int data[], int size, int startPos, int EndPos) {    
    while(startPos <= EndPos){
    int middle = std::floor((startPos + EndPos) / 2);

        if(data[middle] < valueToFind)  {
            startPos = middle + 1;
        }
        else if (data[middle] > valueToFind) {
            EndPos = middle-1;
        }
        else {
            return middle;
        }
    }
    //value not found
    return -1;


}
