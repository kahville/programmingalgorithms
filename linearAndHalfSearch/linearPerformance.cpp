#include <iostream>
/* linear performance*/


/* avagrage compilation time from /usr/bin/time
0.21user 0.04system 0:00.25elapsed 98%CPU (0avgtext+0avgdata 48660maxresident)k
16inputs+32outputs (0major+16013minor)pagefaults 0swaps
*/

/* avarage run time from /usr/bin/time
Linear Search
index of found value is999999
0.00user 0.00system 0:00.01elapsed 92%CPU (0avgtext+0avgdata 7048maxresident)k
0inputs+0outputs (0major+1096minor)pagefaults 0swaps
*/


int LinearSearch(int data[], int size, int searchValue);

int main() {


    int array[1000000];
    int size = (sizeof(array)/sizeof(*array));
    
    //0 -> 999999
    for(int i = 0; i < size; i++)
    {
        array[i] = i;
    }

    int indexOfValue = LinearSearch(array,size, 999999);

    std::cout << "Linear Search" << '\n';
    if(indexOfValue >=0) {
    std::cout << "index of found value is" << indexOfValue << '\n';
    } 
    else {
    std::cout << "value not found" << '\n';
    }        
}

int LinearSearch(int data[], int size, int searchValue) {
  
    for(int i = 0; i < size; i++)
    {
        if(searchValue == data[i]) {
            return i;
        }
    }

    //value not found
    return -1;
}