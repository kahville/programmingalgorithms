// Ville Kähärä - 1401487 - TI14SPELI

//test sort function performance by placing the example code inplace of function block

complie program with:
g++ -o algperformance.out main.cpp -std=c++11

run program with to get run time of the program:
/usr/bin/time ./algperformance.out

/* compile time  tl;dr ~36 milliseconds 
 /usr/bin/time g++ -o algperformance.out main.cpp -std=c++11
0.31user 0.04system 0:00.36elapsed 98%CPU (0avgtext+0avgdata 65960maxresident)k
0inputs+48outputs (0major+20323minor)pagefaults 0swaps
*/

// run time with fill and sort tl;dr ~30 seconds for the size of 100 000
/*
/usr/bin/time ./algperformance.out
fill completed
sort completed
30.23user 0.00system 0:30.31elapsed 99%CPU (0avgtext+0avgdata 3736maxresident)k
0inputs+0outputs (0major+223minor)pagefaults 0swaps
*/