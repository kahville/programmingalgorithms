#include <iostream>
#include <random>

// Ville Kähärä - 1401487 - TI14SPELI

//test sort function performance by placing the example code inplace of function

/* compile time  tl;dr ~36 milliseconds 
 /usr/bin/time g++ -o algperformance.out main.cpp -std=c++11
0.31user 0.04system 0:00.36elapsed 98%CPU (0avgtext+0avgdata 65960maxresident)k
0inputs+48outputs (0major+20323minor)pagefaults 0swaps
*/
// run time with fill and sort tl;dr ~30 seconds for the size of 100 000
/*
/usr/bin/time ./algperformance.out
fill completed
sort completed
30.23user 0.00system 0:30.31elapsed 99%CPU (0avgtext+0avgdata 3736maxresident)k
0inputs+0outputs (0major+223minor)pagefaults 0swaps
*/

void simpleSort(float array[], const int size);

int main () {

    const int size = 100000;
    float testArray[size];
    int arraySize = (sizeof(testArray)/sizeof(*testArray));
    
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(1.0, 2.0);

    for(auto&& i : testArray)
    {
        i =  dis(gen);  
    }
    std::cout << "fill completed " << "\n";
    simpleSort(testArray, arraySize);
    std::cout << "sort completed " << "\n";

}

void simpleSort(float array[], const int size) {
    
    for(int i = 0; i < size; ++i)
        for(int j = 0; j < size; ++j)
            if(array[j] < array[i]) {
                float tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
            }
}
