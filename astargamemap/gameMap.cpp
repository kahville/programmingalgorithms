#include "gameMap.h"

GameMap::GameMap(const std::string &&mapName){
    std::cout << "Opening game map" << '\n';
std::ifstream gamemapFile;
std::string line;
gamemapFile.open(mapName);
std::cout << "reading game map data" << '\n';
std::getline(gamemapFile,line);
 if(line.find(',') != std::string::npos) {
            size_t pos = line.find(',');
            int height = line.at(pos+1) - '0';
            int width = line.at(pos -1) - '0';
            if(height == 0) 
            {
            height =9;
            }
            else if(height ==9) {
                height=0;
            }
            if(width == 0) 
            {
            width =9;
            }
            else if(width ==9) {
                width=0;
            }
            this->height = height;
            this->width = width;
            std::cout << "map size set" << '\n';
}

while(std::getline(gamemapFile,line)) {
 for (int i = 0; i < line.size(); ++i)
  { 
    if(line[i] != ' ' && line[i] !='\n')      // This converts the char into an int and pushes it into vec
    this->rawdata.push_back(line[i] - '0');  // The digits will be in the same order as in original
  }
}
gamemapFile.close();

    int iterator =0;
    for(int i = 0; i < this->height; i++)
    {
        for(int j =0;j < this->width; j++) {
            Point coord(i,j,this->rawdata[iterator]);
            this->mapData.push_back(coord);
            iterator++;
        }
    }
}