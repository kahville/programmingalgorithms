#include "astar.h"

AStar::AStar(Point start, Point goal, GameMap gameMap) {
        
        std::cout << '\n';
        std::cout << "Searching point : ";
        goal.Print();
        std::cout << '\n';

        if(start.m_xCoord < 0 || start.m_yCoord < 0 || start.m_xCoord > gameMap.width || start.m_yCoord > gameMap.height) {
            std::cout << "Map is not large enough, check input" << '\n';
            return;
        }

        if(goal.m_xCoord < 0 || goal.m_yCoord < 0 || goal.m_xCoord > gameMap.width || goal.m_yCoord > gameMap.height) {
            std::cout << "Map is not large enough, check input" << '\n';
            return;
        }

        //find first node
        for(auto&& point : gameMap.mapData)
        {
            if(point.m_xCoord == start.m_xCoord && point.m_yCoord == start.m_yCoord) {
                std::cout << "startPoint found" << '\n';
                unvisitedPoints.push_back(point);
            }
        }

            Point currentLocation;
            std::cout << "set current location" << '\n';
           
        while(!unvisitedPoints.empty()) {
             currentLocation = unvisitedPoints[0];
            for(auto&& pointToCompare : unvisitedPoints)
            {
                 if (pointToCompare.m_dataType <= currentLocation.m_dataType) {
                     currentLocation = pointToCompare;
                 }
            }
            visitedLocations.push_back(currentLocation);            
            unvisitedPoints.pop_back();          

            //fouund the goal return
            if(currentLocation.m_xCoord == goal.m_xCoord && currentLocation.m_yCoord == goal.m_yCoord) {
                std::cout << "goal Found" << '\n';
                
                std::cout << "Visited points:" << '\n';
                for(auto&& visitedPoint : visitedLocations)
                {
                    visitedPoint.Print();
                    std::cout << '\n';

                }
                

                return;
            }

            //generate new search locations
            std::cout << "generate new search locations" << '\n';
            std::vector<Point> adjecentNodes;

            for(auto&& point : gameMap.mapData)
            {
                //side checks
                if(
                    (point.m_xCoord == currentLocation.m_xCoord -1 && point.m_yCoord == currentLocation.m_yCoord) ||
                    (point.m_xCoord == currentLocation.m_xCoord +1 && point.m_yCoord == currentLocation.m_yCoord) ||
                    (point.m_xCoord == currentLocation.m_xCoord && point.m_yCoord == currentLocation.m_yCoord -1) ||
                    (point.m_xCoord == currentLocation.m_xCoord && point.m_yCoord == currentLocation.m_yCoord +1)
                ) 
                {
                    if(
                        (point.m_xCoord >=0 && point.m_xCoord <= gameMap.width) &&
                        (point.m_yCoord >=0 && point.m_yCoord <= gameMap.height)
                    )
                    {
                    std::cout << "Added adj node" << '\n';
                    adjecentNodes.push_back(point);
                    }
                   
                }
            }

            std::cout << "parse adjNodes" << '\n';
            for(auto&& adjPoint : adjecentNodes)
            {
               std::cout << "parse adjPoint" << '\n';
               for(auto&& visitedPoint : visitedLocations)
               {
                   if(adjPoint.m_xCoord == visitedPoint.m_xCoord && adjPoint.m_yCoord == visitedPoint.m_yCoord) {
                        std::cout << "adjNode visited" << '\n';
                        continue;
                   }
               }

               
               for(auto&& upoint : unvisitedPoints)
               {
                   if (adjPoint.m_xCoord == upoint.m_xCoord && adjPoint.m_yCoord == upoint.m_yCoord && upoint.m_dataType > adjPoint.m_dataType) 
                   {
                       std::cout << "adjNode added already" << '\n';
                       continue;
                   }
               }

               std::cout << "found unvisited adj node" << '\n';
               unvisitedPoints.push_back(adjPoint);
                
            }
                
        }
}