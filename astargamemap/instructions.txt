//Ville kähärä 1401487

/*
astar implementation for game map. Astar harjoitus
lukee mapin oikein, mutta kömpelösti. onnistuu hakemaan halutun pisteen x:n arvoilla 0-3 ja y:n arvoilla 0-2
*/
//käännä komennolla
g++ -std=c++14 main.cpp gameMap.cpp astar.cpp point.cpp -o astar.out
//aja komennolla 
./astar.out

/* lähteet

Easy A* (star) Pathfinding
https://medium.com/@nicholas.w.swift/easy-a-star-pathfinding-7e6689c7f7b2

A* Search Algorithm
https://www.geeksforgeeks.org/a-search-algorithm/
*/