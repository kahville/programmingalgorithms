#ifndef GAMEMAP_H
#define GAMEMAP_H
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "point.h"

class GameMap {
    private:
    std::vector<int> rawdata;
    public:
    GameMap(const std::string &&mapName);
    
    std::vector<Point> mapData;
    int width;
    int height;

    
   


};

#endif // GAMEMAP_H