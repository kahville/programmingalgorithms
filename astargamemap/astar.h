
#ifndef ASTAR_H
#define ASTAR_H

#include <iostream>

#include <queue>

#include "point.h"
#include <vector>

#include "gameMap.h"

class AStar
{
private:

    /* data */
    std::vector<Point> unvisitedPoints;
    std::vector<Point> visitedLocations;

public:
AStar(Point start, Point goal,GameMap gamemap);

};

#endif // ASTAR_H
