
#ifndef POINT_H
#define POINT_H

class Point
    {
        public:
        int m_xCoord;
        int m_yCoord;
        int m_dataType;
        Point(int xcoord, int ycoord,int dataType);
        Point();
        Point(int xcoord, int ycoord);
        void Print();

};

#endif // POINT_H