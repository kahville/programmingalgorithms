        #include "point.h"
        #include <iostream>


Point::Point(int xcoord,int ycoord,int dataType) {
m_xCoord=xcoord;
m_yCoord=ycoord;
m_dataType = dataType;
}
Point::Point(int xcoord, int ycoord)
{
m_xCoord=xcoord;
m_yCoord=ycoord;
m_dataType=0;
}
Point::Point() {
        m_xCoord=0;
        m_yCoord=0;
        m_dataType=0;
}

void Point::Print() {
        std::cout << "(x: " << m_xCoord << ", y: " << m_yCoord << ") " << "data: " << m_dataType;
}