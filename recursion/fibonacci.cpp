#include <iostream>
#include <vector>

/*calculate fibonacci numbers*/

//use array/vector to help with iteration.
//expample  fib number is the sum of 2 previous numbers in an array.

/*
Ville Kähärä
1401487
*/

/* 
compile time 
/usr/bin/time g++ -o fibonacci.out fibonacci.cpp
0.27user 0.04system 0:00.32elapsed 98%CPU (0avgtext+0avgdata 57360maxresident)k
8inputs+64outputs (0major+18578minor)pagefaults 0swaps
*/

/*
runtime output
fib number is: 433494437
fib iter is : 433494437
2.04user 0.00system 0:02.04elapsed 99%CPU (0avgtext+0avgdata 3296maxresident)k
0inputs+0outputs (0major+123minor)pagefaults 0swaps
*/
long RekFib(const int n);
long IterFib(const int n);

int main() {

    long fib_rek = RekFib(42);
    std::cout << "fib number is: " << fib_rek << '\n';

    long fib_iter = IterFib(42);
    std::cout << "fib iter is : " << fib_iter << '\n';

}

long RekFib(const int n) {
    if(n == 0) return 1;
    if(n==1) return 1;
    return RekFib(n-1) + RekFib(n-2);
}

long IterFib(const int n) {
    std::vector<long> fib_vector = {1,1};

    if(n == 0) return fib_vector.at(0);

    if(n==1) return fib_vector.at(1);
    
    
    long fibonacci = fib_vector.at(1);

    for(int i = 1; i <=n; i++)
    {
        fibonacci = fib_vector.at(i) + fib_vector.at(i-1); 
        fib_vector.push_back(fibonacci);
    }

    return fib_vector.back();

}



