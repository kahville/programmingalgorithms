#include <iostream>

/*
Ville Kähärä
1401487
*/
//acckerman function
//implement accerman fuction and test Ackerman(2,2)

/*
compilation time Ackerman A(2,2)
/usr/bin/time g++ -o ackerman.out ackerman.cpp -std=c++11
0.21user 0.03system 0:00.24elapsed 98%CPU (0avgtext+0avgdata 46772maxresident)k
0inputs+32outputs (0major+15521minor)pagefaults 0swaps


runtime Ackerman A(2,2)
Ack number is 7
0.00user 0.00system 0:00.00elapsed 50%CPU (0avgtext+0avgdata 3360maxresident)k
0inputs+0outputs (0major+124minor)pagefaults 0swaps
*/

long Ackerman(const int m, const int n);

int main() {


    long ackerman_number = Ackerman(2,2);
    std::cout << "Ack number is " << ackerman_number << '\n';

}

long Ackerman(const int m, const int n) {

    if(m == 0) return n +1;
    if(n == 0) return Ackerman(m-1,1);
    return Ackerman(m-1,Ackerman(m,n-1));
}
