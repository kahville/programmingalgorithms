#ifndef NODE_H
#define NODE_H
#include <iostream>

template <typename T>
class Node
{
public:
    T m_data;
    Node* m_next;
    Node(T data) : m_data(data),m_next(nullptr) {}
};

#endif // NODE_H
