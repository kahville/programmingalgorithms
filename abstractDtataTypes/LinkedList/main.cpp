#include <iostream>
#include "linkedList.h"

int main() {

    LinkedList linkedL;

    std::cout << " adding..." << '\n';

    for(int i=1; i<100000; i++) {
        linkedL.insertNode(i);
    }


    std::cout << " deleting..." << '\n';
    for(int i =1; i<100000; i++) {
        linkedL.deleteNode(i);
    }


    if(linkedL.isEmpty()) {
        std::cout << "Tyhjä" << '\n';
    } else {
        std::cout << "Dataa on" << '\n';
        linkedL.printList();
    }

    
}