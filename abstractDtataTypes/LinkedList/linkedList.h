#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "node.h"
#include <iostream>
#include <memory>

class LinkedList {

    public:
    Node* m_head;
    LinkedList();
    bool isEmpty();
    void insertNode(const int data);
    void printList();
    bool findValue(const int value);
    
    bool deleteNode(const int value);



};
#endif // LINKEDLIST_H
