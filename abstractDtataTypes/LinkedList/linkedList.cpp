#include "linkedList.h"
LinkedList::LinkedList() : m_head(nullptr) {}

bool LinkedList::isEmpty() {
    if(this->m_head == nullptr) return true;
    else {
        return false;
    }
}

//Lisää uuden alkion listan alkuun
void LinkedList::insertNode(const int data) {
    Node *newNode = new Node();
    newNode->m_data = data;
    newNode->m_next = this->m_head;
    this->m_head = newNode;
}

void LinkedList::printList() {
    Node *tmp = this->m_head;
    while(tmp != nullptr) {
        std::cout << tmp->m_data << '\n';
        tmp = tmp->m_next;
    }
}

bool LinkedList::findValue(const int value) {
    Node *tmp = this->m_head;
    while(tmp != nullptr) {
        if(tmp->m_data == value) return true;
        tmp = tmp->m_next;
    }
    return false;
}
    bool LinkedList::deleteNode(const int value) {
        if(this->isEmpty()) return false;

        if(this->m_head->m_data == value) {
            Node *tmp = this->m_head;
            this->m_head = this->m_head->m_next;
            delete tmp;
            return true;
        }

        Node *prev = this->m_head;
        Node *tmp = this->m_head->m_next;
        while(tmp != nullptr) {

        if(tmp->m_data == value){
            
            prev->m_next = tmp->m_next;
            delete tmp;
            return true;
        } 



            prev = tmp;
            tmp = tmp->m_next;

        }


        return false;
    }

