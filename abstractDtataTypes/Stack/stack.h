#ifndef STACK_H
#define STACK_H
#include <iostream>
#include "node.h"
template <typename S>
class Stack {

    private:
    Node<S> *m_head;

    public:

    Stack() : m_head(nullptr) {};

bool isEmpty() {
    if(nullptr == this->m_head ) return true;
    else {
        return false;
    }
}

void print() {
    if(isEmpty()) return;
    Node<S> *tmp = this->m_head;
    while(tmp != nullptr) {
        std::cout << tmp->m_data << '\n';
        tmp = tmp->m_next;
    }
}


void clear() {
    //Node<S> *tmp = this->m_head;
    while(!isEmpty()) {
        pop();
    }
}
    void push(S value) {
        Node<S> *newNode = new Node<S>(value);
        newNode->m_next = m_head;
        m_head = newNode;
    }

    S pop() {
        if(isEmpty()) return S();
        Node<S> *tmp = m_head;
        m_head = m_head->m_next;
        S data = tmp->m_data;
        delete tmp;
        return data;
    }

    S top() {
         if(isEmpty()) return S();
        return this->m_head->m_data;

    }
};




#endif //STACK_H

