#ifndef HEAP_H
#define HEAP_H

#include <iostream>
//ville kähärä 1401487
class Heap {
    private:
    int data[255]= {0};

    public:
    Heap() {}

    bool isEmpty() {
        if(data[0]==0){
        return true;
        } else {
        return false;
        }
    }
    void clear() {
        data[255] ={0};
    }
    
    int delMin() {
        if(isEmpty()) return -1;
        int retVal = data[1];
        data[0] = data[0]-1;
        if(isEmpty()) return retVal;

        data[1] = data[data[0]]; //get last index

        int left_child = 2;
        int right_child = 3;

        while(!isEmpty()) {
                //lapset olemassa
                //jompu kumpi pienempi

            if(data[left_child] < data[right_child]) {
               if(data[left_child] <  data[1]) {
               data[1] = data[data[left_child]];
               break;
               }

            } else {
                if(data[right_child] <  data[1]) {
               data[1] = data[data[right_child]];
               break;
                }
            }

            if(data[1] >= data[data[0]]) {
                int tmp = data[1];
                data[1] = data[data[0]];
                data[data[0]] = tmp;
                left_child++;
                right_child++;
            }

        }


        return retVal;

    }
    void print() {
        
        for(int i = 1; i < data[0]+1; i++)
        {
            std::cout << data[i] << " ";
        }
        
    }

    bool insert( const int value) {
        //liikaa alkioita??
        if( 255 == data[0] ) return false;

        //laitetaan viimeiseksi
        int index = data[0]+1; //pitää kirjaa menokohtaa uuden alkion paikasta
        data[index] = value;
        //kasvata heapin kokoa
        data[0]=data[0]+1;

        //tarkistetaan keko-ominaisuus

        while(index > 1) {
                int parent_index = index/2;

                if(data[parent_index] <= data[index] ) {
                break; //keko ok
                }
                //swap
                int tmp = data[parent_index];
                data[parent_index] = data[index];
                data[index] = tmp;

                //päivitä index
                index=parent_index;
        }
        return true;
    }

};

#endif /* HEAP_H */