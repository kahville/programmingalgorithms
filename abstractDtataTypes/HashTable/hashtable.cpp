#include "hashtable.h"
#include <cmath>
HashTable::HashTable(const int maxItems) 
{
       data.resize(maxItems);
}

void HashTable::insert(const std::string &str) 
{
    int sum =0;
    for(int i = 0; i < str.length(); i++)
    {
        sum += (int(str[i]) - 'a' +1) * std::pow(27,i);
    }
    int hx = sum % data.size();
    data[hx] = str;
}


bool HashTable::findValue(const std::string &str) 
{

    int sum =0;
    for(int i = 0; i < str.length(); i++)
    {
        sum += (int(str[i]) - 'a' +1) * std::pow(27,i);
    }
    int hx = sum % data.size();
    
       for(int index = hx; index < data.size(); index++)
    {
        if(data[index].compare(str) ==0) return true;
        if(data[index+1].empty()) return false;
        if(data[index+2].empty()) return false;
    }
    return false;
}

void HashTable::print() {

    for (int i =0; i < data.size(); i++) {
        std::cout << "index " + std::to_string(i) <<  " data " << data[i] << '\n';
    }
}
