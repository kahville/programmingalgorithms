#ifndef HASTABLE_H
#define HASTABLE_H

#include <iostream>
#include <string>
#include <vector>
//ville kähärä 1401487
//g++ main.cpp hashtable.cpp -o hashtest.out -std=c++14
class HashTable
{
private:
    std::vector<std::string> data;
public:
    HashTable(const int maxItems);
    void insert(const std::string &str);
    bool findValue(const std::string &str);

    void print();
};

#endif // HASTABLE_H
