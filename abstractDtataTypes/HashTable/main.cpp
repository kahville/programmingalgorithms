#include <iostream>
#include "hashtable.h"
//ville kähärä 1401487
//g++ main.cpp hashtable.cpp -o hashtest.out -std=c++14
int main() {
    HashTable hsTable(13);

    //insert values
    std::vector<std::string> valuesToInsert = {"table", "chair", "grass", "floor", "phone", "paint", "enemy", "frost", "angry", "eagle"};

    for(auto&& value : valuesToInsert)
    {
        hsTable.insert(value);
    }

    //find values
    std::vector<std::string> valuesToFind = {"crash", "brass", "birds", "eagle"};

    for(auto&& i : valuesToFind)
    {
        if(hsTable.findValue(i)) {
            std::cout << '\n'<< "found " << i << '\n';
        }
        else {
            std::cout << " not found ";
        }

    }

    std::cout << '\n';
    hsTable.print();
}