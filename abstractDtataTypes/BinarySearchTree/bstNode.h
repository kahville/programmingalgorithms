#ifndef BSTNODE_H
#define BSTNODE_H
#include <iostream>

template <typename T>
class BSTNode
{
public:
    T m_data;
    BSTNode* m_left;
    BSTNode* m_right;
    BSTNode(T data) : m_data(data),m_left(nullptr), m_right(nullptr) {}
};

#endif // BSTNODE_H
