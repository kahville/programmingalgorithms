#ifndef BINARYSEARCHTREE_H
#define BINARYSEARCHTREE_H
#include <iostream>
#include "bstNode.h"

//ville kähärä 1401487
//complie with g++ binarySearchTree.h bstNode.h main.cpp -std=c++14

template <typename S>
class BinarySearchTree
{

  private:
    BSTNode<S> *m_root;

    void traversePrint(BSTNode<S> *currentNode)
    {
        if (currentNode != nullptr)
        {
          
            std::cout << "traversing left: " << '\n';
            traversePrint(currentNode->m_left);
              std::cout << "data: " << currentNode->m_data << '\n';
            std::cout << "traversing right: " << '\n';
            traversePrint(currentNode->m_right);
        }
        std::cout << "returning." << '\n';
    }

  public:
    BinarySearchTree() : m_root(nullptr){};

    void insert(S value)
    {
        if (nullptr == m_root)
        {
            BSTNode<S> *newNode = new BSTNode<S>(value);
            m_root = newNode;
            std::cout <<"inserted root" << m_root->m_data << '\n';
            return;
        }
        BSTNode<S> *currentNode = m_root;
        while (currentNode != nullptr)
        {
            if (value < currentNode->m_data)
            {
                currentNode = currentNode->m_left;
                if (currentNode == nullptr)
                {
                    BSTNode<S> *newDataNode = new BSTNode<S>(value);
                    currentNode = newDataNode;
                    std::cout <<"inserted node left" << currentNode->m_data << '\n';
                    return;
                }
            }
            if (value > currentNode->m_data)
            {
                currentNode = currentNode->m_right;
                if (currentNode == nullptr)
                {
                    BSTNode<S> *newDataNode = new BSTNode<S>(value);
                    currentNode = newDataNode;
                    std::cout <<"inserted node right" << currentNode->m_data << '\n';
                    return;
                }
            }
        }
    }

    void print()
    {
        traversePrint(m_root);
    }
};

#endif //BINARYSEARCHTREE_H
