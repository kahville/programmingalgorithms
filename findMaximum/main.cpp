#include <iostream>

/*find maximum element from array */
//index is the first element. return index and change the loop.

/* compilation time from /usr/bin/time
0.22user 0.02system 0:00.25elapsed 98%CPU (0avgtext+0avgdata 48720maxresident)k
0inputs+32outputs (0major+15981minor)pagefaults 0swaps
*/

/* run time from /usr/bin/time
0.00user 0.00system 0:00.00elapsed 100%CPU (0avgtext+0avgdata 3188maxresident)k
0inputs+0outputs (0major+121minor)pagefaults 0swaps
*/

int findMax(int data[], int size);


int main() {


    int array[5] = {8,2,3,4,5};

    int indexOfMaxValue = findMax(array,5);

    std::cout << "index of max value is" << indexOfMaxValue << '\n';
    std::cout << "maximum value of the array is " << array[indexOfMaxValue] << '\n';
}

int findMax(int data[], int size) {

    int index=0;
    int maxValue =data[0];

    for(int i = 1; i < size; i++)
    {
        if(data[i] > maxValue) {
            maxValue = data[i];
            index = i;
        }

    }
    return index;

}