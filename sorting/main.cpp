#include <iostream>
#include <vector>

//ville kähärä 1401487
//g++ main.cpp -std=c++14
//does not work, time and brains not computing.

std::vector<int> shellSort(std::vector<int> arrayToSort);

int main () {

std::vector<int> array = {9,2,6,5,7,1};

std::vector<int> sortedArray = shellSort(array);


    std::cout << "Sorted array" << '\n';
    for(auto&& i : sortedArray)
    {
        std::cout << i << " ";
    }
    
}


std::vector<int> shellSort(std::vector<int> arrayToSort) {
    std::vector<int> gaps;
     //good gaps
    // 7 3 1
    //109 41 19 5 1
     std::vector<int> gapsSmall = {7,3,1};
     std::vector<int> gapsBig = {109,41,19,5,1};

     if(arrayToSort.size() < 1000) {
     gaps = gapsSmall;
     }
     else {
     gaps = gapsBig;
     }

    
        
        for(auto&& gap : gaps)
       {
         for(int i = gap; i >= arrayToSort.size(); i++)
         {
             int temp = arrayToSort[i];

            int j;
            for(j = i; j >=gap && arrayToSort[j-gap] > temp; j-=gap)
            {
                arrayToSort[j] = arrayToSort[j-gap];
            }
            
            arrayToSort[j] = temp;
         }
       }
    return arrayToSort;  
}